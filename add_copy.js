function text_to_clipboard(text) {
    let Y = window.pageYOffset;
    let textArea = document.createElement("textarea");
    textArea.value = text;
    textArea.id = "clipboard";
    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();
    scrollTo(0, Y);
    document.execCommand('copy');
    document.getElementById("clipboard").remove();
}

console.log("eee");


if ($(".copy_text").length == 0) {
    $("head").append("<style> .copy_text{background-color: #96a028}</style>")

    $(".AviaResultRoute__Selector > .AviaResultRoute__Row").append('<button class="Button Button--primary AviaResultListGroup__BookingButton copy_text" type="button"><div class="Button__Content"><span class="Button__Text"><span>Копировать</span></span></div></button>');
    $(".copy_text").on("click", function() {
        event.stopPropagation();
        let wrapper = $(this).parent().parent().parent().parent().parent();
        let FlightTitle = wrapper.parent().parent().find(".AviaResultListGroup__Routes__RouteGroup__FlightTitle__Airports")
        console.log(FlightTitle);
        let transfer_city = wrapper.find(".AviaResultRoute__TransferCity");
        let CarrierName = wrapper.find(".AviaResultRoute__CarrierName");
        let firstDepartureTime = wrapper.find(".AviaResultRoute__Column.AviaResultRoute__Column--firstDepartureTime");
        let duration = wrapper.find(".AviaResultRoute__Column.AviaResultRoute__Column--duration");
        let lastDepartureTime = wrapper.find(".AviaResultRoute__Column.AviaResultRoute__Column--lastDepartureTime");
        let renderFlight = wrapper.find(".AviaResultRoute__Column.AviaResultRoute__Column--renderFlight > div");
        let ServiceClass = wrapper.find(".AviaResultRoute__ServiceClass");
        let Places = wrapper.find(".AviaResultRoute__Places > div > .AviaResultRoute__PlaceSeats");
        let str = "";
        for (i = 0; i < CarrierName.length; i++) {
            if (renderFlight.eq(i).text().indexOf("<div") != -1) {
                renderFlight = renderFlight.substring(0, renderFlight.indexOf("<div")).replace(/\n|\s{2,}/g, "")
                OtherSupplier = wrapper.find(".AviaRoute__OtherSupplier").text();
                renderFlight += " " + OtherSupplier;
            }
            if (i == 0) {
                str += FlightTitle.text() + " ";
            }
            if (i > 0) {
                str += transfer_city.eq(i - 1).text() + " ";
            }
            str += CarrierName.eq(i).text().replace(/\n|\s{3,}/g, "") + " " + firstDepartureTime.eq(i).text().replace(/\n|\s{3,}/g, "") + " " + duration.eq(i).text().replace(/\n|\s{3,}/g, "") + " " + lastDepartureTime.eq(i).text().replace(/\n|\s{3,}/g, "") + " " + renderFlight.eq(i).text().replace(/\n|\s{3,}/g, "") + " " + ServiceClass.eq(i).text().replace(/\n|\s{3,}/g, "") + " " + "Мест по тарифу " + Places.eq(i).text().replace(/\n|\s{3,}/g, "");
            str += "\n";
        }
        //console.log(CarrierName);
        //console.log(renderFlight);

        //let copy_text = CarrierName + " " + firstDepartureTime + " " + duration + " " + lastDepartureTime + " " + renderFlight + " " + ServiceClass + " " + Places;
        //copy_text = copy_text.replace(/\n|\s{2,}/g, "")
        text_to_clipboard(str);

    });
}